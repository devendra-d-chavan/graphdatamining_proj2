/*
 * main.cpp
 *
 *  Created on: Nov 15, 2013
 *      Author: xczou
 */

#include    <errno.h>
#include    <math.h>
#include    <stdio.h>
#include    <stdlib.h>
#include    <string.h>
#include    <unistd.h>
#include    <ctype.h>
#include    <getopt.h>
#include    <assert.h>
#include    <stdint.h>
#include    <mpi.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <igraph/igraph.h>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_statistics.h>

// Safe free
#define FREE(x) if (x) {free((void*)(x)); (x) = NULL;}

#define NUMSTAT 5

typedef double * signature;

typedef double ** featureMatrix;

double dclock(void) {
	struct timeval tv;
	gettimeofday(&tv, 0);

	return (double) tv.tv_sec + (double) tv.tv_usec * 1e-6;
}

//dividing a number of processes into a number of groups
void grouping(MPI_Comm worldComm, int worldRank, int groupSize ,  /*OUT*/ int *groupID , /*OUT*/ MPI_Comm *groupComm){

	*groupID = worldRank / groupSize;
	int key = worldRank % groupSize;
	MPI_Comm_split(worldComm, *groupID, key, groupComm);
}

//every 'strided' process forms a group
void strided_grouping(MPI_Comm worldComm, int worldSize, int strided,  /*OUT*/  MPI_Group *group,  /*OUT*/  MPI_Comm *groupComm) {
	int ranges[1][3] = { { 0, worldSize - 1, strided } };
	MPI_Group worldGroup;
	MPI_Comm_group(worldComm, &worldGroup);
	MPI_Group_range_incl(worldGroup, 1, ranges, group);
	MPI_Comm_create(worldComm, (*group), groupComm);
}

/*
 * Returns the pointer to the igraph object by loading the edge list from the
 * file
 *
 * Arguments:
 *      graph_filepath: Path to the graph in edge list format
 *      graph:          Pointer to the igraph object
 *
 * TODO: switch to MPI_IO
 */
void load_graph(char * graph_filepath, igraph_t* graph) {
	FILE* graph_file = 0;
	graph_file = fopen(graph_filepath, "r");

	if (!graph_file){
		printf("can not find graph input file %s \n", graph_filepath);
	}
	assert(graph_file != 0);

	int num_vertices, num_edges, ret_val;
	ret_val = fscanf(graph_file, "%d", &num_vertices);
	ret_val = fscanf(graph_file, "%d\n", &num_edges);

	ret_val = igraph_read_graph_edgelist(graph, graph_file, num_vertices, 0);

	// Convert to undirected graph
	ret_val = igraph_to_undirected(graph, IGRAPH_TO_UNDIRECTED_COLLAPSE, 0);

	assert(ret_val == 0);
	fclose(graph_file);

	assert(igraph_vcount(graph) == num_vertices);
	assert(igraph_ecount(graph) == num_edges);

}

void print_vector(igraph_vector_t *v, FILE *f) {
  long int i;
  for (i=0; i<igraph_vector_size(v); i++) {
    fprintf(f, " %li", (long int) VECTOR(*v)[i]);
  }
  fprintf(f, "\n");
}

/*
 * each process extracts the feature matrix for a range a vertex
 */
void extract_node_features(const igraph_t* graph, int startV, int endV, /*OUT*/ featureMatrix *M , int numFeatures) {


	igraph_vector_t degreeVec, v_feat_2, v_feat_3;

    int procV=endV-startV +1; //total vertex this core will process

    igraph_vector_init(&degreeVec, procV);
    igraph_vector_init(&v_feat_2, procV);
    igraph_vector_init(&v_feat_3, procV);

    igraph_vs_t vexSt;
    //this process is processing verteice from startV to endV
    igraph_vs_seq(&vexSt, startV, endV);
    // Feature 1: degree(i)
    igraph_degree(graph, &degreeVec, vexSt, IGRAPH_ALL, IGRAPH_NO_LOOPS);


    // Feature 2: Clustering coefficient
    igraph_transitivity_local_undirected(graph, &v_feat_2, vexSt, IGRAPH_TRANSITIVITY_ZERO);

    // Feature 3: Average 2 hop away neighbors
    igraph_avg_nearest_neighbor_degree(graph, vexSt, &v_feat_3, NULL,NULL);
    igraph_vector_t neighbors,neighborCC;
	igraph_vector_init(&neighbors, 1);
	igraph_vector_init(&neighborCC, 1); //

	igraph_vector_t degOfsubgraph;
	igraph_vector_init(&degOfsubgraph,1);

	int  i = 0, ret_val;

	double degree = 0;               // feature 1
	double  clustering_coeff = 0;    //feature 2
    double avg_2_node_neighbors = 0; //feature 3
    double avg_clustering_coeff = 0; //feature 4
    int egonet_edge_count = 0;       //feature 5
    int egonet_outgoing_edges = 0;   //feature 6
    size_t num_egonet_neighbors = 0; //feature 7

    for (int vid = startV; vid <= endV ; vid ++) {
           degree = VECTOR(degreeVec)[vid-startV];
           clustering_coeff = 0;
           avg_2_node_neighbors = 0;
		   avg_clustering_coeff = 0;
		   egonet_edge_count = 0;
		   egonet_outgoing_edges = 0;
		   num_egonet_neighbors = 0;

          if (degree > 0) {

        	  igraph_vs_t vs;
        	  igraph_vector_ptr_t onehopNeighbors, twohopNeighbors;
			  igraph_vector_ptr_init(&onehopNeighbors, 1);
   			  igraph_vector_ptr_init(&twohopNeighbors, 1);

        	  clustering_coeff = VECTOR(v_feat_2)[vid-startV];
        	  avg_2_node_neighbors = VECTOR(v_feat_3)[vid-startV];

			  ret_val = igraph_neighbors(graph, &neighbors, vid, IGRAPH_ALL);

			  //calculate feature 4, average of clustering coeff of neighbors of node i
			  int numNeighbor = igraph_vector_size(&neighbors);
			  double sum = 0;
			  int neighborVid ;
			  for (int i = 0; i < numNeighbor ; i ++) {
				  igraph_vs_t tmp;
				  neighborVid = VECTOR(neighbors)[i];
				  igraph_vs_1(&tmp, neighborVid);
				  igraph_transitivity_local_undirected(graph, &neighborCC, tmp,  IGRAPH_TRANSITIVITY_ZERO);
				  sum += (VECTOR(neighborCC)[0]);
				  igraph_vs_destroy(&tmp);
			  }

			  avg_clustering_coeff = sum / degree;

			  ret_val = igraph_vector_insert(&neighbors, igraph_vector_size(&neighbors), vid);
			  igraph_vs_t vertex_set = igraph_vss_vector(&neighbors);

			  igraph_t induced_subgraph;
			  // Use IGRAPH_SUBGRAPH_CREATE_FROM_SCRATCH as it is more efficient
			  // when the induced graph has a small number of nodes
			  ret_val = igraph_induced_subgraph(graph, &induced_subgraph,
				  vertex_set, IGRAPH_SUBGRAPH_CREATE_FROM_SCRATCH);

			  // Feature 5: |E_ego(i)| - Number of edges in ego(i)
			  // Graph induced by the neighbors contains the current vertex
			  egonet_edge_count = igraph_ecount(&induced_subgraph);

			  // Feature 6:
			  //calculate sum of degree of all neighbors, including node itself
			  igraph_degree(graph, &degOfsubgraph, vertex_set, IGRAPH_ALL, IGRAPH_NO_LOOPS);
			  double totalDegOfSubgraph= igraph_vector_sum(&degOfsubgraph);
			  int totalEdgesOfSubgraph = igraph_ecount(&induced_subgraph);
			  egonet_outgoing_edges = totalDegOfSubgraph - 2*totalEdgesOfSubgraph;
//			  printf("node %d, totalDeg %f, total edges %d \n", vid, totalDegOfSubgraph, 2*totalEdgesOfSubgraph);

			  // Feature 7: |N(ego(i))| - Number of neighbors of ego(i)
			  igraph_vs_1(&vs,vid);

			  igraph_neighborhood(graph, &onehopNeighbors,vs, 1, IGRAPH_OUT);
			  igraph_neighborhood(graph, &twohopNeighbors,vs, 2, IGRAPH_OUT);

			  num_egonet_neighbors =  igraph_vector_size((igraph_vector_t *)(VECTOR(twohopNeighbors)[0])) -
					  	  	  	  	  igraph_vector_size((igraph_vector_t *)(VECTOR(onehopNeighbors)[0]) );

/*
			  printf("node %d one-hop neighbor %ld, first vector size %ld; ", vid,
					  igraph_vector_ptr_size(&onehopNeighbors),
					  igraph_vector_size((igraph_vector_t *)(VECTOR(onehopNeighbors)[0])) );
			  printf("node %d two-hop neighbor %ld , first vector size %ld.", vid, igraph_vector_ptr_size(&twohopNeighbors),
					  igraph_vector_size((igraph_vector_t *)(VECTOR(twohopNeighbors)[0])));
			  printf("\n");
*/
			  igraph_destroy(&induced_subgraph);
			  igraph_vs_destroy(&vs);
			  igraph_vector_ptr_destroy(&onehopNeighbors);
			  igraph_vector_ptr_destroy(&twohopNeighbors);
          }
		  i = vid - startV  ;
		  int fidx = 0;
		  (*M)[fidx++][i] = degree;
		  (*M)[fidx++][i] = clustering_coeff;
		  (*M)[fidx++][i] = avg_2_node_neighbors;
		  (*M)[fidx++][i] = avg_clustering_coeff;
		  (*M)[fidx++][i] = egonet_edge_count;
		  (*M)[fidx++][i] = egonet_outgoing_edges;
		  (*M)[fidx++][i] = num_egonet_neighbors;
    }

    igraph_vector_destroy(&degreeVec);
    igraph_vector_destroy(&v_feat_2);
    igraph_vector_destroy(&v_feat_3);
    igraph_vector_destroy(&neighbors);
    igraph_vector_destroy(&neighborCC);
    igraph_vector_destroy(&degOfsubgraph);
}

void feature_aggregation(double * data, int num, signature s){
    gsl_sort(data, 1, num);

    // Calculate the feature aggregates
    double median = gsl_stats_median_from_sorted_data(data, 1, num);
    double mean = gsl_stats_mean(data, 1, num);

    // Use the versions that reuses the previously computed values
    double sd = gsl_stats_sd_m(data, 1, num, mean);

    double skewness = 0;
    double kurtosis = 0;

    if (sd > 0) {
		skewness = gsl_stats_skew_m_sd(data, 1, num, mean, sd);
		kurtosis = gsl_stats_kurtosis_m_sd(data, 1, num, mean, sd);
    }

    s[0] = median;
    s[1] = mean;
    s[2] = sd;
    s[3] = skewness;
    s[4] = kurtosis;

}

/*
 * every process takes a same number of vertex to process
 * The last process may take extra (remain of the vertex) to process
 */

void graph_partition(igraph_t *graph, int graphProcSize, int currentProc, /*OUT*/ int * startV, /*OUT*/ int * endV){
	igraph_integer_t totalVertexNum = igraph_vcount(graph);
	//vertex starts with 0
	igraph_integer_t vertexSeg = totalVertexNum / graphProcSize;
	//vertex division works as follows:
	//i.e., 55 vertex in the graph
	//if having 3 processes, then, P0 process vertex [0, 17], [18,35],[35,54]
	*startV = currentProc * vertexSeg ;
	*endV = (currentProc +1 )* vertexSeg  -1 ;
	if (currentProc == graphProcSize -1 ){
		if (totalVertexNum % graphProcSize > 0) { // there is remainder of the graph vertices
			*endV = totalVertexNum -1;
		}
	}
}


void extract_signature_one_proc(char * filepath, int numFeatures, /*OUT*/ signature *sg){
	signature s = (*sg);
	igraph_t graph;
	load_graph(filepath, &graph);
	igraph_integer_t totalVertexNum = igraph_vcount(&graph);
	featureMatrix M;
	M = (featureMatrix) calloc(numFeatures , sizeof(double*));

    for (int i = 0; i < numFeatures; i ++) {
		M[i] = (double *) calloc(totalVertexNum , sizeof(double));
    }

	extract_node_features(&graph, 0, totalVertexNum-1, &M, numFeatures);

    for (int i = 0; i < numFeatures; i ++) {
	  feature_aggregation(M[i], totalVertexNum, (s+i*NUMSTAT));
    }

    for (int i = 0; i < numFeatures; i ++) {
    	FREE(M[i]);
    }
    FREE(M);
    igraph_destroy(&graph);
}

void extract_signature(char * filepath, MPI_Comm graphComm, int numFeatures, /*OUT*/ signature *sg) {
	signature s = (*sg);
	igraph_t graph;

	//each process load the graph,
	load_graph(filepath, &graph);
	// alternative, one process loads the graph, and broadcasts it to other group member

	//Once every process has the graph data,
	//we partition the graph, each of process in charge of feature extraction of a range of vertex
    //calculate the vertex range to be processed by one process
	int graphProcRank, graphProcSize;
	MPI_Comm_rank(graphComm, &graphProcRank);
	MPI_Comm_size(graphComm, &graphProcSize);

	int startV, endV;
	graph_partition(&graph, graphProcSize, graphProcRank, &startV, &endV);

	int numVertexProc = endV - startV + 1;
	igraph_integer_t totalVertexNum = igraph_vcount(&graph);


    featureMatrix M;
//    int totalFeatureValues=(endV - startV + 1) * numFeatures ;
//    M = (featureMatrix) calloc(totalFeatureValues * sizeof(double));
    M = (featureMatrix) calloc(numFeatures , sizeof(double*));
    for (int i = 0; i < numFeatures; i ++) {
    	M[i] = (double *) calloc(numVertexProc , sizeof(double));
    }
    extract_node_features(&graph, startV, endV, &M, numFeatures);

    //collecting the feature matrix
    double *rbuf;
    int *disp, *rcounts;
    int groupLeader = 0;
    if (graphProcRank == groupLeader)  { // process 0 as the group leader
        rbuf = (double *) calloc(totalVertexNum, sizeof(double));
        rcounts = (int*) calloc(graphProcSize, sizeof(int));
        disp = (int *) calloc(graphProcSize, sizeof(int));
        for (int p=0; p < graphProcSize; p++){ // builds rcounts
        	rcounts[p] = numVertexProc;
        }
        for(int p = 1; p < graphProcSize; p++){ // builds disp
           disp[p] = disp[p-1]+numVertexProc;
        }
    }


    for (int i = 0; i < numFeatures; i ++) {
    		MPI_Gatherv(M[i],numVertexProc ,MPI_DOUBLE, rbuf, rcounts, disp, MPI_DOUBLE,
    			groupLeader, graphComm);
    		//feature matrix is gathered
    		//now, it is aggregated on the graphLeader core.
    		if (graphProcRank == groupLeader){
    			feature_aggregation(rbuf, totalVertexNum, (s+i*NUMSTAT));
    			memset(rbuf, 0, sizeof(double) * totalVertexNum);
    		}
    }

    if (graphProcRank == groupLeader){
    	FREE(disp);
		FREE(rcounts);
		FREE(rbuf);
    }
    for (int i = 0; i < numFeatures; i ++) {
    	FREE(M[i]);
    }
    FREE(M);
    igraph_destroy(&graph);
}



double graph_compare(signature s1, signature s2, int oneSigSize) {
  double distance = 0;
  // Calculate the Canberra distance of the two signature vectors
  for (int i = 0; i < oneSigSize; i++)
    {
      double denom = s1[i] + s2[i];
      if (denom != 0)
        distance += fabs(s1[i] - s2[i]) / denom;
    }

  return distance;
}


void graph_adajcent_compare(int numGraphs, int signDim , double * all_signs, /*OUT*/ double ** distances){

    *distances = (double *) calloc(numGraphs-1, sizeof(double));
    double * distanceVec = *distances;
    printf("similarity : ");
	for(int i = 0; i < numGraphs -1; i++){
		double distance = graph_compare(&all_signs[i*signDim], &all_signs[(i+1)*signDim-1], signDim);
		distanceVec[i] = distance;
		printf("%f,", distance);
	}
	printf("\n");

}
