#include "../impl.c"

void test_gatherv(int argc, char *argv[]){

    MPI_Init(&argc, &argv);
	int rank, size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	featureMatrix M;
	int numFeatures = 2 ;
	int procV=3;
	int remain=2;
	if (rank == size -1)
		procV += remain;
	M = (featureMatrix) calloc(numFeatures , sizeof(double*));
	for (int i = 0; i < numFeatures; i ++) {
		M[i] = (double *) calloc(procV, sizeof(double));
	}

	for (int i = 0; i < numFeatures; i ++) {
		for(int j = 0; j < procV; j ++){
			M[i][j] = i*100 + j + rank;
		}
	}

	double *rbuf;
	int *disp, *rcounts;
	int groupLeader = 0;
	int totalV = procV * size + remain;
	if (rank == groupLeader)  { // process 0 as the group leader

		rbuf = (double *) calloc(totalV, sizeof(double));

		rcounts = (int*) calloc(size, sizeof(int));
		disp = (int *) calloc(size, sizeof(int));

		//for other processes, calculate their process vertex range

		for (int p=0; p < size; p++){ // builds rcounts

			rcounts[p] = procV;
			if (p == size -1){
				rcounts[p]+= remain;
			}
			if ( p > 0){
				disp[p] = disp[p-1]+ rcounts[p-1];
			}
		}

	}

	for (int i = 0; i < numFeatures; i ++) {
			MPI_Gatherv(M[i], procV ,MPI_DOUBLE, rbuf, rcounts, disp, MPI_DOUBLE,
				groupLeader, MPI_COMM_WORLD);
			if (rank == groupLeader){
				for(int j = 0; j < totalV; j ++){
					printf("%f,\n", rbuf[j]);
				}
				memset(rbuf, 0, sizeof(double) * totalV);
			}

	//				feature_aggregation(rbuf[i], totalVertexNum, (s+i*NUMSTAT));
	}

	if (rank == groupLeader){
		FREE(rbuf);
		FREE(rcounts);
		FREE(disp);

		for (int i = 0; i < numFeatures; i ++) {
				FREE(M[i]);
		}
		FREE(M);
	}



	MPI_Finalize();
}


void test_group(int argc, char *argv[]){

	    MPI_Init(&argc, &argv);
		int rank, size;
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);
		MPI_Comm_size(MPI_COMM_WORLD, &size);
	    int root = 0;
		if (argc < 3) {
			if (rank == 0) {
				printf(
						"Usage: ./anomaly_detect <graph dataset directory path> <graph name/suffix> <no. of graphs> \n");
			}
			MPI_Finalize();
			return ;
		}

		char graphPath[1024];
		char graphName[256];
		char basePath[512];
		strcpy(basePath, argv[1]);
		int numGraphs = atoi(argv[3]); // number of graph, each graph is processed by a group of process
		strcpy(graphName, argv[2]);

		//form group
		// # of group = numGraph
		//group id is = total process % numGraph
		MPI_Comm graphComm;
		int groupSize = size / numGraphs ;
		int groupID;
		grouping(MPI_COMM_WORLD, rank, groupSize, &groupID, &graphComm);

		//Now, each graph is processed by a group of processes
		//feature is extracted by a set of groups
		sprintf(graphPath, "%s/%d_%s.txt", basePath, groupID, graphName);

		igraph_t graph;
		load_graph(graphPath, &graph);

		int graphProcRank, graphProcSize;
		MPI_Comm_rank(graphComm, &graphProcRank);
		MPI_Comm_size(graphComm, &graphProcSize);
		int startV, endV;
		igraph_integer_t totalVertexNum = igraph_vcount(&graph);
		graph_partition(&graph, graphProcSize, graphProcRank, &startV, &endV);


		MPI_Comm graphLeaderComm;
		MPI_Group graphLeaderGroup;
	    strided_grouping(MPI_COMM_WORLD, size, groupSize /*strided size*/, &graphLeaderGroup, &graphLeaderComm);
	    if (graphLeaderComm != MPI_COMM_NULL){
	    	int graphLeaderSize, graphLeaderRank;
	    	MPI_Comm_rank(graphLeaderComm, &graphLeaderRank);
	    	MPI_Comm_size(graphLeaderComm, &graphLeaderSize);
	    	printf("%d, group ID[%d], GLRank[%d], GLSize[%d], GV[%d], PV[%d,%d] \n", rank, groupID, graphLeaderRank, graphLeaderSize, igraph_vcount(&graph), startV, endV);

	    }else{
	    	printf("%d, group ID[%d], GV[%d], PV[%d,%d] \n", rank, groupID, igraph_vcount(&graph), startV, endV);
	    }


		int numVertexProc = endV - startV + 1;
	    featureMatrix M;
	    int numFeatures = 7 ;
	    M = (featureMatrix) calloc(numFeatures , sizeof(double*));
	    for (int i = 0; i < numFeatures; i ++) {
	    	M[i] = (double *) calloc(numVertexProc , sizeof(double));
	    }
	    extract_node_features(&graph, startV, endV, &M, numFeatures);

	   /* if (rank == 1){
	    	printf("%d -> # of egonet of node 168 = %f \n", rank, M[1][76]);
	    }*/

	    if (rank == 0){
	    	// print feature matrix
	    	for (int i = 0; i < numFeatures; i ++) {
	    		printf("feauture%d: ", i+1);
	    		for(int n = startV ; n <= endV ; n ++){
	    			printf("%f,", M[i][n-startV]);
	    		}
	    		printf("\n");
	    	}

	    }

	    //collecting the feature matrix
		double *rbuf;
		int *disp, *rcounts;
		int groupLeader = 0;
		if (graphProcRank == groupLeader)  { // process 0 as the group leader

			rbuf = (double *) calloc(totalVertexNum, sizeof(double));
			rcounts = (int*) calloc(graphProcSize, sizeof(int));
			disp = (int *) calloc(graphProcSize, sizeof(int));

			//for other processes, calculate their process vertex range

			for (int p=0; p < graphProcSize; p++){ // builds rcounts
				int sv, ev ;
			    graph_partition(&graph, graphProcSize, p, &sv, &ev);
				rcounts[p] = (ev-sv +1);
				if ( p > 0){
					disp[p] = disp[p-1]+ rcounts[p-1];
				}
			}
			/*if (rank == 0) {
				printf ("%d, GPRank %d \n", rank, graphProcRank);

				for (int p=0; p < graphProcSize; p++){ // builds rcounts
					printf("%d,",rcounts[p]);
				}
				printf("\n");
				for (int p=0; p < graphProcSize; p++){ // builds rcounts
					printf("%d,",disp[p]);
				}
				printf("\n");
			}*/

		}

		signature s= (signature) calloc(numFeatures * NUMSTAT , sizeof(double));

		for (int i = 0; i < numFeatures; i ++) {
				MPI_Gatherv(M[i], numVertexProc ,MPI_DOUBLE, rbuf, rcounts, disp, MPI_DOUBLE,
					groupLeader, graphComm);
				if (graphProcRank == groupLeader){
					feature_aggregation(rbuf, totalVertexNum, (s+i*NUMSTAT));
					memset(rbuf, 0, sizeof(double) * totalVertexNum);
				}

		}

		if (rank == 0){
			printf("signature: ");
			for(int  i = 0; i < numFeatures * NUMSTAT; i ++){
				printf("%f, ", s[i]);
			}
			printf("\n");

		}


		/*if (graphProcRank == groupLeader){
			printf("[%d], signature of graph [%d] ", rank, groupID);
			for(int i = 0; i < numFeatures * NUMSTAT ; i ++){
				printf("%f,", s[i]);
			}
			printf("\n");
		}*/

		FREE(s);

		if (graphProcRank == groupLeader){
			FREE(disp);
			FREE(rcounts);
			FREE(rbuf);
		}
		for (int i = 0; i < numFeatures; i ++) {
			FREE(M[i]);
		}
		FREE(M);
		igraph_destroy(&graph);

		MPI_Finalize();
}

int main(int argc, char *argv[]) {
	test_group(argc, argv);
//	test_gatherv(argc, argv);
}
