
#include "impl.c"

int main(int argc, char *argv[]) {
	MPI_Init(&argc, &argv);
	int rank, size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
    int root = 0;
	if (argc < 4) {
		if (rank == 0) {
			printf(
					"Usage: ./anomaly_detect <graph dataset directory path> <graph name/suffix> <no. of graphs> <output filepath>\n");
		}
		MPI_Finalize();
		return 1;
	}

	double start = 0, fextractionS=0 , fextractionE = 0 ,  compS=0;
	if (rank == root){
		start = dclock();
		fextractionS = dclock();
	}
	char graphPath[1024];
	char graphName[256];
	char basePath[512];
	char output_filepath[1024];
	strcpy(basePath, argv[1]);
	strcpy(graphName, argv[2]);
	int numGraphs = atoi(argv[3]); // number of graph, each graph is processed by a group of process
	strcpy(output_filepath, argv[4]);
	float upper_threshold_multiplier = atof(argv[5]);

	int numFeature = 7;
	// TODO: refactor following codes
	// if number of graph is less than the total process number, then,
	// we have multiple processes working on one graph.
	if (numGraphs < size ){
		//form group
		// # of group = numGraph
		//group id is = total process % numGraph
		if (rank == 0 )
			printf("start one graph parallelism (multiple processes working on same graph )\n");
		MPI_Comm graphComm;
		int groupSize = size / numGraphs ;
		int groupID;
		grouping(MPI_COMM_WORLD, rank, groupSize, &groupID, &graphComm);

		//Now, each graph is processed by a group of processes
		//feature is extracted by a set of groups
		sprintf(graphPath, "%s/%d_%s.txt", basePath, groupID, graphName);
		signature s ;
		s= (signature) calloc(numFeature * NUMSTAT , sizeof(double));
		//every graph signature is collected at graphGroupLeader process
		extract_signature(graphPath, graphComm, numFeature, &s);

		MPI_Barrier(MPI_COMM_WORLD);
		if (rank == root){
			fextractionE = dclock();
			compS = dclock();
		}

		MPI_Comm graphLeaderComm;
		MPI_Group graphLeaderGroup;
		strided_grouping(MPI_COMM_WORLD, size, groupSize /*strided size*/, &graphLeaderGroup, &graphLeaderComm);
		// collect signatures to the root process

		double *sigRecvBuf; // holds signatures from all graphs
		int *rcounts, *disp;
		int signDim = numFeature * NUMSTAT; // signature dimension : 7 * 5
		if (rank == root){
			//every process has fixed size of signature
			sigRecvBuf = (double *) calloc(numGraphs * signDim , sizeof(double) );
			rcounts = (int *) calloc(numGraphs, sizeof(int));
			disp = (int *) calloc(numGraphs, sizeof(int));
			for (int i =0; i < numGraphs; i++){
				rcounts[i] = signDim;
			}
			for (int i =1; i < numGraphs; i++){
				disp[i] = disp[i-1] + signDim;
			}
		}

		if (graphLeaderComm != MPI_COMM_NULL){
			MPI_Gatherv(s,  signDim ,MPI_DOUBLE, sigRecvBuf, rcounts, disp, MPI_DOUBLE,
							root, graphLeaderComm);
		}

		if (rank == root){
			// calculate the distance score btw/ adjacent graphs
			double * distanceVec;
			graph_adajcent_compare(numGraphs, signDim, sigRecvBuf, &distanceVec);
			// Create a copy of the vector before in place sorting
                        double *copyDistanceVec = (double *) calloc(numGraphs-1, sizeof(double));
                        for(int i = 0; i < numGraphs - 1; i++)
                        {
                            copyDistanceVec[i] = distanceVec[i];
                        }

                        gsl_sort(copyDistanceVec, 1, numGraphs - 1);
                        double median = gsl_stats_median_from_sorted_data(copyDistanceVec, 1, numGraphs - 1);
                        double sd = gsl_stats_sd(copyDistanceVec, 1, numGraphs - 1);

                        printf("median = %f, sd = %f, numGraphs = %d\n", median, sd, numGraphs);

                        // Use only the upper threshold
                        double upper_threshold = median +
                                                  upper_threshold_multiplier*sd;

                        FILE* output_file = 0;
                        output_file = fopen(output_filepath, "w");
                        fprintf(output_file, "%f NaN\n", upper_threshold);

                        int anomaly_count = 0;
                        // distanceVec[i] -> similarity(G_i, G_{i+1})
                        // Graphs G_0 and G_n are NOT anomalous
                        for(int i = 1; i < numGraphs - 2; i++)
                        {
                            if(distanceVec[i-1] > upper_threshold &&
                               distanceVec[i] > upper_threshold)
                            {
                              fprintf(output_file, "%d %f %f\n", i,
                                      distanceVec[i-1], distanceVec[i]);
                              anomaly_count++;
                            }
                        }


                        fclose(output_file);

                        FREE(distanceVec);
                        FREE(copyDistanceVec);

                        printf("Percentage of anomalies = %f%%\n", (anomaly_count*100.0/numGraphs));
		}

		if (rank == root){
			FREE(sigRecvBuf);
			FREE(rcounts);
			FREE(disp);
		}
		FREE(s);
	}else {

		if (rank == 0 )
			printf("start one process parallelism (multiple graphs processed by one process )\n");

		//if we have a lot of graphs, we have one process processing multiple graphs
		int graphProcessed = numGraphs / size; // # of graphs processed by one process
		if ((rank == size - 1) && (numGraphs % size > 0) ) {
				graphProcessed += (numGraphs % size); // last process takes care of remain graphs
		}
		int graphStart = rank * graphProcessed;
		int graphEnd = (rank + 1) * graphProcessed  - 1 ;
        if (graphEnd >= numGraphs){
        	graphEnd = numGraphs -1; //graph start with 0
        }
//		printf("%d, process input graphs[%d,%d]", rank, graphStart, graphEnd);

		// all signatures in one single array to simply the data gathering process
		int signDim = numFeature * NUMSTAT; // signature dimension : 7 * 5
		double * all_signs = (double *) calloc(graphProcessed*signDim, sizeof(double));
		for (int g = graphStart; g <= graphEnd; g ++){
			sprintf(graphPath, "%s/%d_%s.txt", basePath, g, graphName);
			double *sigs = all_signs+ (g-graphStart)*signDim ;
			extract_signature_one_proc(graphPath, numFeature, &sigs);
		}

		MPI_Barrier(MPI_COMM_WORLD);
		if (rank == root){
			fextractionE = dclock();
		}

		double *sigRecvBuf; // holds signatures from all graphs
		int *rcounts, *disp;
		if (rank == root){
			//every process now has multiple signatures
			sigRecvBuf = (double *) calloc(numGraphs * signDim , sizeof(double) );
			rcounts = (int *) calloc(size, sizeof(int));
			disp = (int *) calloc(size, sizeof(int));
			int gp = numGraphs / size; // do calculation again
			for (int i =0; i < size; i++){
				if ((i == size - 1 ) && ( numGraphs % size > 0)){
					rcounts[i] = (gp + (numGraphs % size)) * signDim;
				}else {
					rcounts[i] = gp*signDim;
				}
			}
			for (int i =1; i < size; i++){
				disp[i] = disp[i-1] + rcounts[i-1];
			}
		}

		MPI_Gatherv(all_signs,  graphProcessed*signDim ,MPI_DOUBLE, sigRecvBuf, rcounts, disp, MPI_DOUBLE,
							root, MPI_COMM_WORLD);

		MPI_Barrier(MPI_COMM_WORLD);

		if (rank == root){
				compS = dclock();
		}

		if (rank == root){
			// calculate the distance score btw/ adjacent graphs
			double * distanceVec;// = (double *) calloc(numGraphs-1, sizeof(double));
			graph_adajcent_compare(numGraphs, signDim, sigRecvBuf, &distanceVec);

			// Create a copy of the vector before in place sorting
			double *copyDistanceVec = (double *) calloc(numGraphs-1, sizeof(double));
			for(int i = 0; i < numGraphs - 1; i++)
                        {
			    copyDistanceVec[i] = distanceVec[i];
                        }

			gsl_sort(copyDistanceVec, 1, numGraphs - 1);
			double median = gsl_stats_median_from_sorted_data(copyDistanceVec, 1, numGraphs - 1);
			double sd = gsl_stats_sd(copyDistanceVec, 1, numGraphs - 1);

			printf("median = %f, sd = %f, numGraphs = %d\n", median, sd, numGraphs);

			// Use only the upper threshold
			double upper_threshold = median +
			                          upper_threshold_multiplier*sd;

			FILE* output_file = 0;
			output_file = fopen(output_filepath, "w");
			fprintf(output_file, "%f NaN\n", upper_threshold);

			int anomaly_count = 0;
			// distanceVec[i] -> similarity(G_i, G_{i+1})
			// Graphs G_0 and G_n are NOT anomalous
			for(int i = 1; i < numGraphs - 1; i++)
                        {
			    if(distanceVec[i-1] > upper_threshold &&
			       distanceVec[i] > upper_threshold)
                            {
                              fprintf(output_file, "%d %f %f\n", i,
                                      distanceVec[i-1], distanceVec[i]);
                              anomaly_count++;
                            }
                        }


			fclose(output_file);

			FREE(distanceVec);
			FREE(copyDistanceVec);

			printf("Percentage of anomalies = %f%%\n", (anomaly_count*100.0/numGraphs));
		}

		if (rank == root){
				FREE(sigRecvBuf);
				FREE(rcounts);
				FREE(disp);
		}

		FREE(all_signs);


	}

	if (rank == root){
		printf("time: %f [%f,%f] \n", dclock() - start, fextractionE-fextractionS,dclock()-compS);
	}
	MPI_Finalize();
}

