/*
 * graph_compare.cpp
 *
 *  Created on: Nov 15, 2013
 *      Author: ksrivas, ddchavan
 */
#include <vector>
#include <math.h>

using namespace std;
typedef vector<double> signature;

/*
 * Returns the Canberra distance between the 2 signatures
 *
 * Arguments:
 *      s1:     Pointer to the first signature
 *      s2:     Pointer to the second signature
 *
 * Return:
 *      Value containing the Canberra distance beween s1 and s2
 */
double
graph_compare(signature* s1, signature* s2)
{
  double distance = 0;
  // Calculate the Canberra distance of the two signature vectors
  for (int i = 0; i < s1->size(); i++)
    {
      double denom = fabs(s1->at(i)) + fabs(s2->at(i));
      if (denom != 0)
        distance += fabs(s1->at(i) - s2->at(i)) / denom;
    }

  return distance;
}
