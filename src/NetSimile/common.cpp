/*
 * common.cpp
 *
 *  Created on: Nov 15, 2013
 *      Author: xczou, ddchavan
 */

#include <assert.h>
#include <igraph/igraph.h>
#include <cstdio>
#include <string>

using namespace std;

/*
 * Returns the pointer to the igraph object by loading the edge list from the
 * file
 *
 * Arguments:
 *      graph_filepath: Path to the graph in edge list format
 *      graph:          Pointer to the igraph object
 */
void
load_graph(string graph_filepath, igraph_t* graph)
{
  FILE* graph_file = 0;
  graph_file = fopen(graph_filepath.c_str(), "r");

  assert(graph_file != 0);

  int num_vertices, num_edges, ret_val;
  ret_val = fscanf(graph_file, "%d", &num_vertices);
  ret_val = fscanf(graph_file, "%d\n", &num_edges);

  ret_val = igraph_read_graph_edgelist(graph, graph_file, num_vertices, 0);

  // Convert to undirected graph
  ret_val = igraph_to_undirected(graph, IGRAPH_TO_UNDIRECTED_COLLAPSE, 0);

  assert(ret_val == 0);
  fclose(graph_file);

  assert(igraph_vcount(graph) == num_vertices);
  assert(igraph_ecount(graph) == num_edges);

}

void
destroy_graph(igraph_t* graph)
{
  igraph_destroy(graph);
}
