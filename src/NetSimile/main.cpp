/*
 * main.cpp
 *
 *  Created on: Nov 15, 2013
 *      Author: xczou
 */

#include <iostream>
#include <vector>
#include <set>
#include <stdint.h>
#include <cassert>
#include <cstdio>
#include <cstring>
#include <string>
#include <algorithm>
#include <map>
#include <sys/time.h>
#include <string>
#include <sstream>

#include <igraph/igraph.h>

#include "common.cpp"
#include "feature_extraction.cpp"
#include "feature_aggregation.cpp"
#include "graph_compare.cpp"

using namespace std;

string
convertInt(int number)
{
  stringstream ss;
  ss << number;
  string str = ss.str();
  return str;
}

void
print_signatures(vector<signature>* s)
{
  for (vector<signature>::iterator iter = s->begin(); iter != s->end(); ++iter)
    {
      for (signature::iterator inner_iter = iter->begin();
          inner_iter != iter->end(); ++inner_iter)
        {
          cout << *inner_iter << " ";
        }
      cout << endl;
    }
}

void
print_vector(vector<double> *vec)
{
  for (vector<double>::iterator iter = vec->begin(); iter != vec->end(); ++iter)
    {
      cout << *iter << " ";
    }
}

void
test()
{
  igraph_t graph;
  vector<signature> signatures;
  signature s1;

  matrix feature_matrix;
  load_graph("../../datasets/enron/0_enron_by_day.txt", &graph);
  extract_node_features(&graph, &feature_matrix);
  aggregate_features(&feature_matrix, &s1);
  signatures.push_back(s1);

  signature s2;
  feature_matrix.clear();
  load_graph("../../datasets/enron/1_enron_by_day.txt", &graph);
  extract_node_features(&graph, &feature_matrix);
  aggregate_features(&feature_matrix, &s2);
  signatures.push_back(s2);

  print_signatures(&signatures);

  double d = graph_compare(&signatures.at(0), &signatures.at(1));
  cout << d << endl;
}

string build_filepath(string dirpath, string graph_name, int day)
{
    string filepath;
   filepath.append(dirpath);
   filepath.append("/");
   filepath.append(convertInt(day));
   filepath.append("_");
   filepath.append(graph_name);
   filepath.append(".txt");

   return filepath;
}

signature
get_signature(string filepath)
{
  signature s;
  matrix feature_matrix;
  igraph_t graph;

  load_graph(filepath.c_str(), &graph);

  feature_matrix.clear();

  extract_node_features(&graph, &feature_matrix);
  aggregate_features(&feature_matrix, &s);
  destroy_graph(&graph);

  return s;
}

int
main(int argc, char *argv[])
{
  if (argc < 3)
    {
      cout << "Usage: ./anomaly_detect <graph dataset directory path> "
          << "<graph name/suffix> <no. of graphs>" << endl;
      return 1;
    }

  //test();
  // TODO: Parallelize!

  vector<igraph_t> graphs;
  string dirpath = argv[1];
  string graph_name = argv[2];
  int num_days = atoi(argv[3]);

  // Reuse the same matrix and graph
  matrix feature_matrix;
  igraph_t graph;

  vector<signature> signatures;

  // Calculate the signature for each graph
  // Do not store intermediate feature matrices
  for (int i = 0; i < num_days; i++)
    {
      string filepath = build_filepath(dirpath, graph_name, i);
      cout << "Processing " << filepath << endl;

      signature s = get_signature(filepath);
      signatures.push_back(s);
    }

  print_signatures(&signatures);

  vector<double> distances;
  // Compare the consecutive signatures
  for (int i = 0; i < signatures.size() - 1; i++)
    {
      double distance = graph_compare(&signatures.at(i), &signatures.at(i + 1));
      distances.push_back(distance);
    }

  print_vector(&distances);

  // TODO: Compute the threshold from the distances and all distances more than
  //            the threshold are anomalous

  return 0;
}

