/*
 * feature_extraction.cpp
 *
 *  Created on: Nov 15, 2013
 *      Author: xczou, ddchavan
 */

#include <igraph/igraph.h>
#include <stddef.h>
#include <algorithm>
#include <cstdio>
#include <iterator>
#include <set>
#include <vector>

#define NUM_FEATURES 7

using namespace std;

typedef vector<vector<double> > matrix;

/*
 * Returns the number of neigbors in the egonet of the induced subgraph
 *
 * Arguments:
 *      graph:                  Pointer to the complete graph
 *      induced_subgraph:       Pointer to the induced subgraph
 *
 * Returns:
 *      The number of neighbors of the induced subgraph
 */
size_t
get_number_egonet_neigbors(const igraph_t* graph, const igraph_vs_t vertex_set)
{
  int ret_val;
  igraph_vector_ptr_t res;
  igraph_vector_ptr_init(&res, 0);

  set<long int> neighbors;
  int order = 1;

  igraph_neighborhood(graph, &res, vertex_set, order, IGRAPH_ALL);
  for (int i = 0; i < igraph_vector_ptr_size(&res); i++)
    {
      igraph_vector_t* v = (igraph_vector_t*) igraph_vector_ptr_e(&res, i);

      for (int j = 0; j < igraph_vector_size(v); j++)
        {
          neighbors.insert(VECTOR(*v)[j]);
        }

      igraph_vector_destroy(v);
      delete (igraph_vector_t *) v;
    }

  set<long int> subgraph_vertices;
  igraph_vit_t vit;

  igraph_vit_create(graph, vertex_set, &vit);
  while (!IGRAPH_VIT_END(vit))
    {
      subgraph_vertices.insert(IGRAPH_VIT_GET(vit));
      IGRAPH_VIT_NEXT(vit);
    }

  vector<long int> unique_vertices;
  set_symmetric_difference(neighbors.begin(), neighbors.end(),
      subgraph_vertices.begin(), subgraph_vertices.end(),
      back_inserter(unique_vertices));
  size_t num_neighbors = unique_vertices.size();
  igraph_vit_destroy(&vit);
  igraph_vector_ptr_destroy(&res);

  return num_neighbors;
}

/*
 * Prints the contents of the igraph vector to the specified FILE
 *
 * Arguments:
 *      v: Pointer to the igraph vector
 *      f: Pointer to the output file
 */
void
print_vector(const igraph_vector_t *v, FILE *f)
{
  long int i;
  for (i = 0; i < igraph_vector_size(v); i++)
    {
      fprintf(f, " %li", (long int) VECTOR(*v)[i]);
    }
  fprintf(f, "\n");
}

/*
 * Returns the feature matrix (node x feature) for the graph
 *
 * Arguments:
 *      graph:          Pointer to the graph
 *      feature_matrix: Pointer to the feature matrix
 *
 * Returns:
 *      None
 */
void
extract_node_features(const igraph_t* graph, matrix* feature_matrix)
{
  igraph_vit_t vit;
  igraph_vit_create(graph, igraph_vss_all(), &vit);

  int ret_val;
  igraph_vector_t v_feat_1, v_feat_2, v_feat_3;

  igraph_vector_init(&v_feat_1, igraph_vcount(graph));
  igraph_vector_init(&v_feat_2, igraph_vcount(graph));
  igraph_vector_init(&v_feat_3, igraph_vcount(graph));

  // Feature 1: degree(i)
  igraph_degree(graph, &v_feat_1, igraph_vss_all(), IGRAPH_ALL,
      IGRAPH_NO_LOOPS);

  // Feature 2: Clustering coefficient
  igraph_transitivity_local_undirected(graph, &v_feat_2, igraph_vss_all(),
      IGRAPH_TRANSITIVITY_ZERO);

  // Feature 3: Average 2 hop away neighbors
  igraph_avg_nearest_neighbor_degree(graph, igraph_vss_all(), &v_feat_3, NULL,
  NULL);

  long int degree = 0;
  double clustering_coeff = 0;
  double avg_2_node_neighbors = 0;
  double avg_clustering_coeff = 0;
  int egonet_edge_count = 0;
  int egonet_outgoing_edges = 0;
  size_t num_egonet_neighbors = 0;

  long int vertex_id;

  while (!IGRAPH_VIT_END(vit))
    {
      vertex_id = IGRAPH_VIT_GET(vit);

      degree = VECTOR(v_feat_1)[vertex_id];
      clustering_coeff = 0;
      avg_2_node_neighbors = 0;
      avg_clustering_coeff = 0;
      egonet_edge_count = 0;
      egonet_outgoing_edges = 0;
      num_egonet_neighbors = 0;

      if (degree > 0)
        {
          clustering_coeff = VECTOR(v_feat_2)[vertex_id];
          avg_2_node_neighbors = VECTOR(v_feat_3)[vertex_id];

          // Feature 5: |E_ego(i)| - Number of edges in ego(i)
          // Graph induced by the neighbors contains the current vertex
          igraph_vector_t v;
          igraph_vector_init(&v, 1);
          ret_val = igraph_neighbors(graph, &v, vertex_id, IGRAPH_ALL);
          ret_val = igraph_vector_insert(&v, igraph_vector_size(&v), vertex_id);
          igraph_vs_t vertex_set = igraph_vss_vector(&v);

          igraph_t induced_subgraph;
          // Use IGRAPH_SUBGRAPH_CREATE_FROM_SCRATCH as it is more efficient
          // when the induced graph has a small number of nodes
          ret_val = igraph_induced_subgraph(graph, &induced_subgraph,
              vertex_set, IGRAPH_SUBGRAPH_CREATE_FROM_SCRATCH);

          egonet_edge_count = igraph_ecount(&induced_subgraph);

          // Feature 7: |N(ego(i))| - Number of neighbors of ego(i)
          num_egonet_neighbors = get_number_egonet_neigbors(graph, vertex_set);
          igraph_destroy(&induced_subgraph);
          igraph_vector_destroy(&v);
        }

      // Set the features in the feature matrix
      vector<double> features;
      features.push_back(degree);
      features.push_back(clustering_coeff);
      features.push_back(avg_2_node_neighbors);
      features.push_back(avg_clustering_coeff);
      features.push_back(egonet_edge_count);
      features.push_back(egonet_outgoing_edges);
      features.push_back(num_egonet_neighbors);

      feature_matrix->push_back(features);

      // Get the next vertex
      IGRAPH_VIT_NEXT(vit);
    }

  igraph_vit_destroy(&vit);
  igraph_vector_destroy(&v_feat_1);
  igraph_vector_destroy(&v_feat_2);
  igraph_vector_destroy(&v_feat_3);
}
