/*
 * feature_aggregation.cpp
 *
 *  Created on: Nov 15, 2013
 *      Author: xczou, ddchavan
 */

#include <vector>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_statistics.h>

#define NUM_FEATURES 7

using namespace std;

typedef vector<double> signature;

/*
 * Return the aggregates of the features in the feature matrix
 *
 * Arguments:
 *      feature_matrix: Pointer to the node x feature matrix
 *      s:              Pointer to the signature of the feature matrix
 *
 * Returns:
 *      None
 */
void
aggregate_features(const matrix* feature_matrix, signature* s)
{
  double* data = NULL;
  for (int feature_index = 0; feature_index < NUM_FEATURES; feature_index++)
    {

      vector<double> feature_col_vector;
      for (matrix::const_iterator  row_iter = feature_matrix->begin();
          row_iter != feature_matrix->end(); ++row_iter)
        {
          vector<double> row = *row_iter;
          feature_col_vector.push_back(row.at(feature_index));
        }

      // Move the values of each feature column to a double[] to compute
      // the aggregates
      if (data == NULL)
        {
          // Allocate memory only once and then reuse as the size remains
          // the same for all the graphs
          data = new double[feature_col_vector.size()];
        }

      size_t num_rows = feature_col_vector.size();
      for (int i = 0; i < num_rows; i++)
        {
          data[i] = feature_col_vector[i];
        }

      gsl_sort(data, 1, num_rows);

      // Calculate the feature aggregates
      double median = gsl_stats_median_from_sorted_data(data, 1, num_rows);
      double mean = gsl_stats_mean(data, 1, num_rows);

      // Use the versions that reuses the previously computed values
      double sd = gsl_stats_sd_m(data, 1, num_rows, mean);

      double skewness = 0;
      double kurtosis = 0;

      if (sd > 0)
        {
          skewness = gsl_stats_skew_m_sd(data, 1, num_rows, mean, sd);
          kurtosis = gsl_stats_kurtosis_m_sd(data, 1, num_rows, mean, sd);
        }

      s->push_back(median);
      s->push_back(mean);
      s->push_back(sd);
      s->push_back(skewness);
      s->push_back(kurtosis);
    }

  if (data != NULL)
    {
      delete data;
    }
}
