CSC 591 Graph Data Mining - Project 2: Anomaly detection in Time-evolving graphs
==============================

Team 12: Xiaocheng Zou [xzou2], Kandarp Srivastava [ksrivas], Devendra D. Chavan [ddchavan]
------------------------
Overview
----------------
Anomaly detection refers to the problem of finding patterns in data which fail 
to conform to the expected standard. There are a variety of anomaly detection 
algorithms available, and the correct one to use depends on the type of anomaly 
you are trying to detect. A few examples are point anomalies, contextual 
anomalies, and collective anomalies. This project will be focused on anomaly 
detection in time evolving graphs. This is a challenging problem that has been 
researched in many different domains, including social networks, computer 
networks (e.g. TCP traffic), and road networks. Various methods have been 
explored, including time series models, similarity-based algorithms, and 
community-based algorithms. Evaluation of these approaches is often not 
straightforward because of the lack of a ground truth. However, we can compare 
the detected anomalies with normal time points in the dataset, or compare 
results among different methods in order to evaluate the performance of the 
methods indirectly.

Goal
----------------
Implement the given anomaly detection algorithm for real-word graphs.  

 1. Implement the algorithm with an **obsession** for attaining the best 
 performance possible.
 2. Analyze, criticize, and evaluate the algorithm and turn in a report on 
 your thoughts and findings.

Required Packages
-----------------
 1. **GNU Statistics** Package 
  
    1. Download the package from [here][1]

    2. Installation
        1. `./configure --prefix=[GSL INSTALL PATH]`
        2. `make -j`
        3. `make install`
        4. [optional] you can do the check by doing following command 
        `make check > log 2>&1`

 2. **Igraph** Package 

    1. Download the package from [here][2]

    2. Installation
        1. `./configure  --prefix=[GSL INSTALL PATH]`
        2. `make -j`
        3. `make install`

 3. **MPI** Package 

    1. Download the package from [here][3]
  
    2. Installation
        1. `./configure  --prefix=[MPI INSTALL PATH]`
        2. `make -j`
        3. `make install`
  
    3. Set Environment Variables
        1. Open `~/.bashrc` 
        2. Export the `[MPI INSTALL PATH]` by adding 
        `export MPIPATH=[MPI INSTALL PATH]` in `bashrc`
        3. Export `MPI` executable command and library by adding following two 
            lines in `bashrc`
           `export PATH=${MPIPATH}/bin:${PATH}`
           `export LD_LIBRARY_PATH=${MPIPATH}/lib:${LD_LIBRARY_PATH}`

Building the program
------------
 
 1. Modify `GSL_PATH` and `IGRAPH_PATH` to your installed paths in Makefile file

 2. Run `make` in `src/NetSimileP` to build the program. This results in the 
    executable `parallel_anomaly_detect`

Usage
-----------
`mpirun -np 4 ./parallel_anomaly_detect <graph dataset directory path> <graph name/suffix> <no. of graphs> <output filepath> <upper threshold multiplier>`

For example,
`mpirun -np 4 ./parallel_anomaly_detect ../../datasets/p2p-Gnutella p2p-Gnutella 9 p2p_out 0.9`
  
This assumes that the graph file name is of the form 
`<seq. no>_p2p-Gnutella.txt` where `<seq. no.>` is a zero based integer. 

In addition, this above example uses *4* process to calculate the similarity 
value (between two adjacent graphs) from 9 graphs. 

The *output file* specifies the anomalous time points as described in the next
section.

The *upper threshold multiplier* defines the multiplier for the standard
deviation used in calculating the upper threshold value of the Canberra 
distance i.e. `upper_threshold = median + <multiplier> * standard_deviation`

A value is anomalous if `distance[i]` and `distance[i+1]` are both greater
than the `upper_threshold`, where `distance[i]` indicates the distance 
between graphs `G_i` and `G_{i+1}`. `G_0` and `G_n` are assumed to be normal.

Output
------------
It consists two main parts (last two lines from the program output). 
First part is the similarity value list. Second part is the total time 
spent on this program and the percentage of anomalous points. 

In addition, the **output file** contains the upper threshold value (in line 1) 
(since the implementation does not require the use of the lower threshold 
value, *NaN* is used a placeholder) and the subsequent lines contains the 
anomalous time point & distance value between the current and the previous & 
next respectively.

The format is as follows:

    upper_threshold NaN
    i <value of time points i and i-1> <value of time points i and i+1>
    j <value of time points j and j-1> <value of time points j and j+1>

Example
-------------

    > mpirun -np 4 ./parallel_anomaly_detect ../datasets/p2p-Gnutella p2p-Gnutella 9 p2p.output 0.06
    start one process parallelism (multiple graphs processed by one process )
    similarity : 27.414712,29.159235,29.280154,29.526045,27.881852,29.000000,0.000000,0.000000,
    median = 28.440926, sd = 13.309853, numGraphs = 9
    Percentage of anomalies = 11.111111%
    time: 46.956570 [46.956274,0.000224] 

**Output file - p2p.output**

    29.239517 NaN
    2 29.280154 29.526045

This can be interpreted as the upper threshold value is `29.239517` and the 
lower threshold value is undefined.
While the first line indicates that the 2nd time point is anomalous with the 
Canberra distance between it and the previous graph (1st graph) is `29.280154`, 
while the distance between it and the next graph (3rd graph) is `29.526045`.

Sample input and output files
-------------
The input files are present in the `datasets` directory, while the results of 
the program execution are present in the `results` directory.

Analyzing the graph structure
-------------
The non isolated vertices of each graph in the dataset are plotted using the 
`igraph.plot()` in `R` (see `src/scripts` directory). The generated plots are 
then viewed to analyze the difference between anomalous and normal graphs.
Also, included in the same directory is a shell script wrapper for generating
the graphs for all the datasets.

**Note** Running the scripts requires `R` with `igraph` package to be 
installed on the system.

  [1]: ftp://ftp.gnu.org/gnu/gsl/gsl-1.9.tar.gz
  [2]: http://sourceforge.net/projects/igraph/files/C%20library/0.6.5/igraph-0.6.5.tar.gz/download
  [3]: http://www.mpich.org/static/downloads/1.5rc3/mpich2-1.5rc3.tar.gz
